<?php
    require_once("config.php");
    require_once(ROOT_PATH."/models/product.php");
    require_once(ROOT_PATH."/models/cart.php");

    if(empty($_SESSION['id'])){
        header("Location: /login.php");
        die();
    }
    if(!empty($_POST)){
        $productId = $_POST['product_id'] ?? 0;
        $productId = (int) $productId;
        if(!$productId){
            header("Location: /index.php");
            die();
        }
        ////
        $product = getProduct($pdo, $productId);
        if(empty($product)){
            header("Location: /index.php");
            die();
        }
        addToCart($pdo, $productId, $_SESSION['id']);
    }
    //select all products from cart

    $cartProduct = getProductFromCart($pdo, $productId);
    
    //display into template
    require_once(ROOT_PATH."/templates/product_details.php");
?>
