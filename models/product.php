<?php 

    function getHomeProducts($db, $categoryId = 0) 
    {
        $filter = '';
        $params = [];
        if($categoryId){ // если $categoryId задана то мы добавляем условие в  переменную $filter 
            $filter .= " AND category_id = :category_id ";
            $params = ["category_id" => $categoryId];
        }
        // если $categoryId не задано то иф пропускается и запрос выполнится без данных условия.
        $stmt = $db->prepare("SELECT * FROM products WHERE 1 ".$filter." ORDER BY RAND() LIMIT 4" );
        $stmt->execute($params); // когда иф нет то подставится пустой массив. 
        $result = $stmt->fetchALL(PDO::FETCH_ASSOC);
        //var_dump($result); //die(); //для проверки кода. 
        // здесь $result массив со всеми полями из таблицы products
        
        return $result;
    
    }

// ф-я принмает подключение к БД Запрос =$stmt первый Выбери все поля из таблицы products сортированные по Случайному выбору =RAND() и выбирай только 4 продукта = LIMIT 4. У нас стоит query обычный запрос потому что этот запрос мы делаем без внесения юзером каких то данныъ он внутренний поэтому не может быть вмешательства в нащ код. Это называется прямой запрос. ПОЛУЧИ ВСЕ=fetchALL в виде ассоциативного массива. и возвращаем мы этот массив

    function getProduct($db, $productId)
    {
        $stmt = $db->prepare("SELECT * FROM products WHERE id = :product_id");
        $stmt->execute(["product_id" => $productId]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

// эта функция получает  конкретный продукт когда нажимаешь на картинку или его название, так называемое более широкое описание продукта. 
 ?>