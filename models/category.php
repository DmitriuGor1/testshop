<?php 

    function getCategories($db)
    {
        $stmt = $db->query("SELECT * FROM categories");
        $result = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $result;
    }

    //  в этой ф-и мы просто возвращаем список с категориями.

    function fetchCategoryTreeList($db, $parent = 0, $UserTreeArr = '')
    {
        if(!is_array($UserTreeArr))
        {
            $UserTreeArr = []; 
        }      
            $sql = "SELECT * FROM `categories` WHERE 1 AND `parent_id` = $parent ORDER BY id ASC";
            $stmt = $db->query($sql);   
            $result = $stmt->fetchALL(PDO::FETCH_ASSOC);
// здесь лежит массив в нем информация о категориях у которых поле parent_id = 0
            
            if(count($result) > 0)
            {
                $UserTreeArr[] = "<ul>";
                foreach($result as $row)
// $row это ассоц массив где название поля ключ и значение
                {
                    $UserTreeArr[] = "<li><a href='/index.php?category=".$row['id']."'>". $row['title']."</a></li>";
                    $UserTreeArr = fetchCategoryTreeList($db, $row['id'], $UserTreeArr);
                }   
                $UserTreeArr[] = "</ul><br>";
            }    
            return $UserTreeArr;
    }

// это рекурсивная ф-я которая вызывает саму себя. и выводит дерево категорий
        
// функция для подсчета времени запроса. 
    
    function rutime($ru, $rus, $index)
    {
        return ($ru["ru_index.tv_sec"] * 1000 + intval($ru["ru_index.u_sec"]/1000)) - ($rus["ru_index.tv_sec"] * 1000 + intval($ru["ru_index.u_sec"]/1000)); 
    }


// функция для подсчета времени запроса. 











    function getCategoriesTree($list, $pk = 'id', $pid = 'parent_id', $child = '_child', $root = 0) 
    {
        $tree = []; //  в этот массив формируется все дерево.
        if (is_array($list)) 
        {
            $refer = [];
            foreach ($list as $key => $data) 
            {
                $refer[$data[$pk]] =& $list[$key];
            }
            // echo '<pre>';
            // print_r($refer); // узнаем как формируется массив $refer[$data[$pk]]
            // echo '<pre>';
            // die();
            foreach ($list as $key => $data) 
            {
                $parentId = $data[$pid];

                // echo '<pre>'; // здесь мы проходимся по нашему массиву из таблицы
                // print_r($parentId); // и вытягиваем из него значение поля parent_id
                // echo '<pre>'; // которое есть у нас в таблице 
                // die();

                if ($root == $parentId) 
                {
                    $tree[] =& $list[$key];

                    // echo '<pre>';
                    // print_r($tree);
                    // echo '<pre>';
                    // die();
                }
                else
                {
                    if (isset($refer[$parentId]))

                        // echo '<pre>';
                        // print_r($refer[$parentId]);
                        // echo '<pre>';
                        // die();
                    {
                        $parent =& $refer[$parentId];
                        $parent[$child][] =& $list[$key];

                        // echo '<pre>';
                        // print_r($parent);
                        // echo '<pre>';
                        // die();
                    }
                }
            }

        }
        return $tree;
    }

//$list это наш массив категорий данных из БД. то что мы получим после fetchALL в виде ассоциативного массива. $refer = []; это вспомагательный массив.  foreach ($list as $key => $data)  перебираем наши поля где ключ это поля а $data значения конкретного поля. $data это у нас одна запись из таблицы. $pk это айдишник записи и формируем $refer[$data[$pk]] вспомогательный массив в котором есть значение конкретного поля например Эдектроникс и это значение является массивом для йадишника. То есть тут мы получаем айдишник с таблицы который соответствует выбранной категории. &$list[$key]; это сама запись в таблицу причем по ссылке то есть мы ее берем уже готовенькую. $parentId = $data[$pid]; здесь мы записываем айдишник родителя.это уже точно 
//if ($root == $parentId если этот айдишник равен нулю то это у нас будет родитель $tree = $list[$key]; Так мы первый раз записываем родителя. Дальше смотрим если у нас запись в $parentId = & $refer[$parentId]; то тогда мы определям родителся $parent = & $refer[$parentId]; и присваиваем через $parent[$child][] = & $list[$key]; новый массив чайлд у нас то префикс. Проходим сначала родителей добавляем а потом дочерниее элементы. 
// $refer[$data[$pk]] это массив где ключ это айдишник категорий. 

// $refer = [];
// foreach ($list as $key => $data)  перебираем наш массив с таблицы категорий, в котором 
// {      // $key это ключи то есть названия полей в таблице КАТЕГОРИИ. и каждая строка превращается таким образом в массив. Но пока это просто много не упорядоченных массивов. 
//     $refer[$data[$pk]] это означает теперь массив будет состоять из значений и конкретного значения [$pk] то есть АЙДИ. Это означает что теперь наши массивы получат индекс по айдишнику из таблицы категорий. 
// & $list[$key]; по ссылке передаем наш массив с ключами названия полей соответствуют значениям. В итоге мы получаем Массив РЕФЕР[] глобальный в котором лежит еще один массив РЕФЕР[$data[$pk]] первые скобки означают глобальный массив а вторые что в нем лежит еще один массив  и этот массив имеет значение $pk = 'id' то есть айди вот и он массив и вот к этому массиву заливается еще весь массив который приходит из базы данных . Ассоциативный массив. По ссылке чтобы его не путать. Снизу пример происходящего  Первые три строчки соответствуют $refer[$data[$pk]], а дальше пошел массив по ссылке. 
//     Array
// // (
//     [1] => Array
//         (
//             [id] => 1
//             [title] => Electronics
//             [parent_id] => 0
//             [created_date] => 2019-11-05 14:17:30
//             [updated_date] => 2019-11-05 14:17:30
//         )
 // [2] => Array
 //        (
 //            [id] => 2
 //            [title] => Books
 //            [parent_id] => 0
 //            [created_date] => 2019-11-05 14:17:39
 //            [updated_date] => 2019-11-05 14:17:39
 //        )

    // Во вторым переборе мы отбираем конкретную значание из поле ПАРЕНТ АЙДИ, и заполняем этим значением переменную Далле если эта переменная равна нулю тогда в дерево вкладывается весь наш ассоциативный массив. Выглядит это так.

// Array
// (
//     [0] => Array
//         (
//             [id] => 1
//             [title] => Electronics
//             [parent_id] => 0
//             [created_date] => 2019-11-05 14:17:30
//             [updated_date] => 2019-11-05 14:17:30
//         )

// )

// а вот если ПАРЕНТ АЙЛИ не ноль тогда мы проверям существует ли массив $refer[$parentId] а внем лежит :
//     Array
// (
//     [id] => 1
//     [title] => Electronics
//     [parent_id] => 0
//     [created_date] => 2019-11-05 14:17:30
//     [updated_date] => 2019-11-05 14:17:30
// )
// тошда переменная $parent =& $refer[$parentId]; по ссылке присваивает этот массив, и наполняется им, далее переменная $parent[$child][] превращается в массив по принципу РЕФЕРЕР с АДИШНИКОМ. Где $parent это массив глобальный с ключем $child и к нему добавляется по ассоциации еще весь массив & $list[$key]; по ссылке, так как у него ПАРЕНТ АЙДИ НЕ НОЛЬ, и так будет пролжаться пока не переберутся все ПАРЕНТ АЙДИ в таблице. 
//     Array
// (
//     [id] => 1
//     [title] => Electronics
//     [parent_id] => 0
//     [created_date] => 2019-11-05 14:17:30
//     [updated_date] => 2019-11-05 14:17:30
//     [_child] => Array  = $parent[$child][] а вторые пустые скобки 
//         (                соответствуют второму массиву ниже он является значеним 
//             [0] => Array $parent[$child] и он передается по ссылке. 
//                 (
//                     [id] => 4
//                     [title] => TV
//                     [parent_id] => 1
//                     [created_date] => 2019-11-07 14:26:55
//                     [updated_date] => 2019-11-07 14:26:55
//                 )

//         )

// )
// в итоге получится такое дерево.

//     Array
// (
//     [0] => Array
//         (
//             [id] => 1
//             [title] => Electronics
//             [parent_id] => 0
//             [created_date] => 2019-11-05 14:17:30
//             [updated_date] => 2019-11-05 14:17:30
//             [_child] => Array
//                 (
//                     [0] => Array
//                         (
//                             [id] => 4
//                             [title] => TV
//                             [parent_id] => 1
//                             [created_date] => 2019-11-07 14:26:55
//                             [updated_date] => 2019-11-07 14:26:55
//                         )

//                     [1] => Array
//                         (
//                             [id] => 5
//                             [title] => Smartphones
//                             [parent_id] => 1
//                             [created_date] => 2019-11-07 14:27:05
//                             [updated_date] => 2019-11-07 14:27:05
//                             [_child] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => 9
//                                             [title] => Apple
//                                             [parent_id] => 5
//                                             [created_date] => 2019-11-07 14:28:10
//                                             [updated_date] => 2019-11-07 14:28:10
//                                         )

//                                 )

//                         )

//                     [2] => Array
//                         (
//                             [id] => 6
//                             [title] => Laptops
//                             [parent_id] => 1
//                             [created_date] => 2019-11-07 14:27:20
//                             [updated_date] => 2019-11-07 14:27:20
//                             [_child] => Array
//                                 (
//                                     [0] => Array
//                                         (
//                                             [id] => 10
//                                             [title] => Sumsung
//                                             [parent_id] => 6
//                                             [created_date] => 2019-11-07 14:28:52
//                                             [updated_date] => 2019-11-07 14:28:52
//                                         )

//                                 )

//                         )

//                 )

//         )

//     [1] => Array
//         (
//             [id] => 2
//             [title] => Books
//             [parent_id] => 0
//             [created_date] => 2019-11-05 14:17:39
//             [updated_date] => 2019-11-05 14:17:39
//             [_child] => Array
//                 (
//                     [0] => Array
//                         (
//                             [id] => 7
//                             [title] => Programming
//                             [parent_id] => 2
//                             [created_date] => 2019-11-07 14:27:40
//                             [updated_date] => 2019-11-07 14:27:40
//                         )

//                     [1] => Array
//                         (
//                             [id] => 8
//                             [title] => Fantasy
//                             [parent_id] => 2
//                             [created_date] => 2019-11-07 14:27:47
//                             [updated_date] => 2019-11-07 14:27:47
//                         )

//                 )

//         )

//     [2] => Array
//         (
//             [id] => 3
//             [title] => Pictures
//             [parent_id] => 0
//             [created_date] => 2019-11-05 16:04:49
//             [updated_date] => 2019-11-05 16:04:49
//         )

// )



// По ссылкам сначала формирует массив РОдителя потом если РООТ не ноль то он формирует массивы детей через РЕФЕРЕР, а потом уже объединят их по ссылкам через детей и таким образом формируется наш огромный массив. 


 ?>