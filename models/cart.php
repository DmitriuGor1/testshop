<?php
    
    function addToCart($db, $productId, $userId)
    {
        $stmt = $db->prepare("INSERT INTO `cart`(`product_id`, `user_id`, `quantity`) VALUES(:product_id, :user_id, 1)");
        $stmt->execute(["product_id" => $productId, "user_id" => $userId]);
        return;
    }
    // с этого момента в таблице КАРТ появится первый продукт. 

    function getProductFromCart($db, $productId)
    {
        $stmt = $db->prepare("SELECT * FROM cart WHERE id = :product_id");
        $stmt->execute(["product_id" => $productId]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    } 



   
?>
