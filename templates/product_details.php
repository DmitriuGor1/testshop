<?php require_once(ROOT_PATH."/templates/header.php");?>
<main role="main" class="inner cover mt-5">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-6">
                <?php include(ROOT_PATH."/templates/product_form.php"); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</main>
<?php require_once(ROOT_PATH."/templates/footer.php");?>
