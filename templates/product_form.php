<div class="card mb-4">
  <div><a href="<?php echo SITE_URL;?>/product_details.php?product_id=<?php echo $product['id'];?>"><img class="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTErn_B3y5sQAjq8OVZ99zgs0LZY9pyk2HyvViNdsAM37kI1KL&s">
  </div>
    <div class="card-body">
      <p class="card-text"><?php echo $product['title'];?></a></p>
      <div class="d-flex justify-content-between align-items-center">
        <div class="btn-group">
            <form method="POST" action="<?php echo SITE_URL. '/cart.php';?>">
                <input type="hidden" name="product_id" value="<?php echo $product['id'];?>">
                <button type="submit" class="btn btn-sm btn-outline-secondary">Order</button>
            </form>
        </div>
        <small class="text-muted"><?php echo $product['price']?></small>
      </div>
    </div>
</div>



<!-- https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTErn_B3y5sQAjq8OVZ99zgs0LZY9pyk2HyvViNdsAM37kI1KL&s -->