<?php require_once(ROOT_PATH.'/templates/header.php'); ?>

<main role="main" class="inner cover">
    <h1 class="cover-heading">Log In</h1>

<form class="form-signin" method="POST" action="<?php echo SITE_URL.'/login.php' ?>">
      <img class="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <div>
          <?php foreach ($errors as $error): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
          <?php endforeach; ?>
      </div>
    <!-- Перед полями формы добавляю доп див и в него вношу отображение ошибок. экшьн на логин.пхп где логика и получается так если пользователь попадает в одну из ошибок, эти ошибки хранятся в массиве и потому в зависимости от ошибки пхп перебирает массив и выводит ошибку сверху над формой -->
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" value="<?php echo (!empty($_POST['inputEmail']) ? $_POST['inputEmail'] : ''); ?>" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
      <br>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
      <br>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2019</p>
</form>
   
</main>

<?php require_once(ROOT_PATH.'/templates/footer.php'); ?>
