<?php 
    require_once('config.php');
    require_once(ROOT_PATH.'/models/user.php');

    use \Nekman\LuhnAlgorithm\LuhnAlgorithmFactory;
    use \Nekman\LuhnAlgorithm\Number;

    // $creditCard = '5q158385360001624';  // эта переменная будет содержать номер кредитной карты. 
    // $luhn = LuhnAlgorithmFactory::create();
    // $number = Number::fromString($creditCard); // удаляет тире пробелы 
    // if ($luhn->isValid($number)) { // проверяем валидность карты. 
    // // Number is valid. если ТРУ то мы можем выводиь...
    //     echo 'Card is valid';
    // } else {
    //     echo 'Not Valid';
    // }
    // die();

    $errors = []; 
    if(!empty($_POST)){
        if(empty($_POST['inputEmail'])){
            $errors[] = 'Please enter email';       
        }
        if(empty($_POST['inputPassword'])){
            $errors[] = 'Please enter password';       
        }

        if (empty($errors)){
            $login = CheckLogin($pdo, SALT, $_POST['inputEmail'], $_POST['inputPassword']);
            //var_dump($login); die(); //для проверки кода 
            if($login){
                $_SESSION['id'] = $login; 
                header("Location:/index.php");
// вот соединение id юзера c таблицы с id .user в сессии. Вот как они приравниваются. 
// redirect если пароль и имейл совпадают значит переводим пользователя на след страницу. 
            } else {
                $errors[] = 'Credentials are invalid';     
            }
        }
    }

    // if(!empty($_POST['inputEmail']) && !empty($_POST['inputPassword'])) на основе написанных выше проверок данная проверка отпала за ненадобностью хотя изначально она помогает держать внимание и показывает как что делается с нуля. ЕЕ оставлю только для того чтобы было пошаговое понимание как все делается. 

    

    require_once(ROOT_PATH.'/templates/login.php');




?>