<?php
    require_once("config.php");
    require_once(ROOT_PATH."/models/product.php");
    require_once(ROOT_PATH."/models/category.php");


    // $productId = 0;
    // if(!empty($_GET['product_id'])){
    //     $productId = $_GET['product_id'];
    // }

    $productId = $_GET['product_id'] ?? 0; //  ИФ сверху то же самое что и в этой строке. 
    $productId = (int) $productId;
    if(!$productId){
        header("Location: /index.php");
        die();
    }
    // если я выбрал продукт то меня переведет на описание продукта. 
    $product = getProduct($pdo, $productId);
    if(empty($product)){
        header("Location: /index.php");
        die();
    }

    require_once(ROOT_PATH."/templates/product_details.php");
?>
